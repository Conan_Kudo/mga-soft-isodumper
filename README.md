isodumper
=========

A tool for USB sticks and removable memory devices like SD-cards.

Features:
* dump ISO file on the device;
* format the device in one partition of ext4, NTFS or FAT32 type;
* backup the device as a whole.


Requirements
-------------

- polkit
- python3
- python3-parted
- python3-pydbus
- udisks2
- libyui and declanation of ncurses, gtk or Qt
- libyui-mga


License
--------

This software is distributed under the terms of the
[GNU General Public License version 2 (GPLv2+)](COPYING.GPL).

The [isodumper icon](isodumper.svg) is licensed under the terms of the
[GNU Lesser General Public License version 2.1 (LGPLv2.1+)](COPYING.LGPL).
It is part of the Crystal Clear icon set by Everaldo Coelho.

Authors
--------
- Papoteur, <papoteur@mageialinux-online.org>


Contributors
--------
- david.david, <david.david@mageialinux-online.org>


Translators
--------
- Papoteur, Guillaume Bernard, Eric Barbero, Rémi Verschelde, Antoine Dumondel - French
- Sergei Zivukov, AlexL, Анатолий Валерианович, Valentin XliN Saikov - Russian
- Yuri Chornoivan - Ukrainian
- marja - Dutch
- motitos, Miguel Ortega, Rodrigo Prieto, Egoitz Rodriguez Obieta - Spanish
- Marcello Anni - Italian
- Atilla ÖNTAŞ, tarakbumba, Fırat Kutlu - Turkish
- Filip Komar - Slovenian
- Kristoffer Grundström, Michael Eklund - Swedish
- You-Cheng Hsieh - Chinese Taiwan
- Marcio Andre Padula - Brazilian Portuguese
- Florin Cătălin RUSSEN - Romanian
- kiki.syahadat - Indonesian
- Daniel Napora, Tatsunoko - Polish
- Oliver Grawert, Rémi Verschelde, Marc Lattemann, user7, Christian Ruesch, psyca - German
- Marek Laane - Estonian
- dtsiamasiotis, Dimitrios Glentadakis - Greek
- fri, Jiří Vírava - Czech
- Rémi Verschelde - Danish
- Richard Shaylor, MrsB, Andi Chandler - English (United Kingdom)
- Manuela Silva - Portuguese
- Ḷḷumex03 - Asturian
- Egoitz Rodriguez Obieta - Basque
- Ardit Dani - Albanian
- xiao wenming - Simplified Chinese
- Cristian Garde Zarza, Davidmp - Catalan
- Ivica  Kolić - Croatian
- Balzamon - Hungarian
- Milan Baša - Slovak
- Linuxero - Arabic
- Masanori Kakura - Japanese
