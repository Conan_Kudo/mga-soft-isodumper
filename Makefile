# Copyright (C) 2013 THE isodumper'S COPYRIGHT HOLDER
# This file is distributed under the same license as the isodumper package.

# This Makefile is free software; the Free Software Foundation
# gives unlimited permission to copy and/or distribute it,
# with or without modifications, as long as this notice is preserved.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY, to the extent permitted by law; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.

# Author isodumper software= papoteur <papoteur@mageialinux-online.org>
# Author Makefile file= Geiger David <david.david@mageialinux-online.org>

PREFIX=/usr
BINDIR=$(PREFIX)/bin
SBINDIR=$(PREFIX)/sbin
LIBDIR=$(PREFIX)/lib
PYLIBDIR=$(shell python3 -c "from __future__ import print_function; from distutils.sysconfig import get_python_lib; print(get_python_lib())")
LIBEXECDIR=$(PREFIX)/libexec
POLKITPOLICYDIR=$(PREFIX)/share/polkit-1/actions
DATADIR=$(PREFIX)/share
ICONSDIR=$(PREFIX)/share/icons
PIXMAPSDIR=$(PREFIX)/share/pixmaps
LOCALEDIR=$(PREFIX)
DOCDIR=$(PREFIX)/share/doc/isodumper
PYTHON=/usr/bin/env python3
SYSCONFDIR=/etc
DIRS = polkit
POFILES = po
DBUSDIR=$(SYSCONFDIR)/dbus-1
SYSTEMDCONFDIR=$(LIBDIR)/systemd/system/

PACKAGE = isodumper
VERSION = 1.04
GITPATH = git://git.mageia.org/software/isodumper

all: dirs pofiles

dirs:
	@for n in . $(DIRS); do \
		[ "$$n" = "." ] || make -C $$n || cd .. || exit 1 ;\
	done

pofiles:
	@for n in . $(POFILES); do \
		[ "$$n" = "." ] || make -C $$n || cd .. || exit 1 ;\
	done

clean:
	rm -f polkit/isodumper polkit/org.mageia.isodumper.policy \
	isodumper.tar.xz


install:

	# for binary file script isodumper on /usr/bin
	mkdir -p $(DESTDIR)$(BINDIR)
	install -m 755 isodumper $(DESTDIR)$(BINDIR)

	# for backend magiback listening DBus on /usr/bin/
	install -m 755 backend/magiback $(DESTDIR)$(BINDIR)

	# for DBus conf file on /etc/dbus-1/system-d
	# to launch magiback on request
	mkdir -p $(DESTDIR)${DBUSDIR}/system.d
	install -m 644 backend/org.mageia.Magiback.conf $(DESTDIR)${DBUSDIR}/system.d

	# for DBus conf file on /usr/share/dbus-1/system-d
	# to provide objects and interfaces
	mkdir -p $(DESTDIR)${DATADIR}/dbus-1/system-services
	install -m 644 backend/org.mageia.Magiback.service $(DESTDIR)${DATADIR}/dbus-1/system-services

	# for systemd service on /usr/lib/systemd/system
	mkdir -p $(DESTDIR)${SYSTEMDCONFDIR}
	install -m 644 backend/magiback.service $(DESTDIR)${SYSTEMDCONFDIR}

	# for policy file isodumper on /usr/share/polkit-1/actions/
	# to have authentication with polkit (use for mageia policy)
	mkdir -p $(DESTDIR)$(POLKITPOLICYDIR)
	install -m 644 polkit/org.mageia.isodumper.policy $(DESTDIR)$(POLKITPOLICYDIR)

	# for PYLIBFILES isodumper.py raw_write.py
	mkdir -p $(DESTDIR)$(PYLIBDIR)/isodumper
	install -m 755 lib/isodumper.py $(DESTDIR)$(PYLIBDIR)/isodumper
	install -m 755 backend/raw_write.py $(DESTDIR)$(PYLIBDIR)/isodumper

	# for LIBFILES raw_format.py
	mkdir -p $(DESTDIR)$(LIBDIR)/isodumper
	install -m 755 lib/raw_format.py $(DESTDIR)$(LIBDIR)/isodumper

	# for manatools mpan file in /etc/mpan/categories.conf.d/
	mkdir -p $(DESTDIR)$(SYSCONFDIR)/mpan/categories.conf.d
	install -m 644 mpan/categories.conf.d/isodumper.conf $(DESTDIR)$(SYSCONFDIR)/mpan/categories.conf.d/isodumper.conf

	# for DATADIR isodumper.py header.png and header.svg
	mkdir -p $(DESTDIR)$(DATADIR)/isodumper
	install -m 644 share/isodumper/header.png $(DESTDIR)$(DATADIR)/isodumper
	install -m 644 share/isodumper/header.svg $(DESTDIR)$(DATADIR)/isodumper

	# for isodumper-qt desktop menu entry
	mkdir -p $(DESTDIR)$(DATADIR)/applications
	install -m 644 share/applications/isodumper.desktop $(DESTDIR)$(DATADIR)/applications/isodumper-qt.desktop
	sed -i -e "s|Name=IsoDumper|Name=IsoDumper (Qt)|" $(DESTDIR)$(DATADIR)/applications/isodumper-qt.desktop
	sed -i -e "s|Exec=isodumper|Exec=isodumper --qt|" $(DESTDIR)$(DATADIR)/applications/isodumper-qt.desktop
	install -m 644 share/applications/format-usb.desktop $(DESTDIR)$(DATADIR)/applications/format-usb-qt.desktop
	sed -i -e "s|Name=Format USB devices|Name=Format USB devices (Qt)|" $(DESTDIR)$(DATADIR)/applications/format-usb-qt.desktop
	sed -i -e "s|Exec=isodumper|Exec=isodumper --qt|" $(DESTDIR)$(DATADIR)/applications/format-usb-qt.desktop

	# for isodumper-gtk desktop menu entry
	install -m 644 share/applications/isodumper.desktop $(DESTDIR)$(DATADIR)/applications/isodumper-gtk.desktop
	sed -i -e "s|Name=IsoDumper|Name=IsoDumper (GTK)|" $(DESTDIR)$(DATADIR)/applications/isodumper-gtk.desktop
	sed -i -e "s|Exec=isodumper|Exec=isodumper --gtk|" $(DESTDIR)$(DATADIR)/applications/isodumper-gtk.desktop
	install -m 644 share/applications/format-usb.desktop $(DESTDIR)$(DATADIR)/applications/format-usb-gtk.desktop
	sed -i -e "s|Name=Format USB devices|Name=Format USB devices (GTK)|" $(DESTDIR)$(DATADIR)/applications/format-usb-gtk.desktop
	sed -i -e "s|Exec=isodumper|Exec=isodumper --gtk|" $(DESTDIR)$(DATADIR)/applications/format-usb-gtk.desktop

	# for isodumper doc
	mkdir -p $(DESTDIR)$(DOCDIR)
	install -m 644 COPYING.GPL COPYING.LGPL CHANGELOG README.md i18n.md $(DESTDIR)$(DOCDIR)

	# for isodumper icons
	#NOTE: You must install imagemagick package.
	mkdir -p $(DESTDIR)$(ICONSDIR)
	convert isodumper.png -geometry 32x32 $(DESTDIR)$(ICONSDIR)/isodumper.png
	mkdir -p $(DESTDIR)$(ICONSDIR)/mini
	convert isodumper.png -geometry 20x20 $(DESTDIR)$(ICONSDIR)/mini/isodumper.png
	mkdir -p $(DESTDIR)$(ICONSDIR)/large
	convert isodumper.png -geometry 48x48 $(DESTDIR)$(ICONSDIR)/large/isodumper.png
	mkdir -p $(DESTDIR)$(PIXMAPSDIR)
	install -m 644 isodumper.png $(DESTDIR)$(PIXMAPSDIR)
	for png in 128x128 64x64 48x48 32x32 22x22 16x16; \
	do \
	mkdir -p $(DESTDIR)$(ICONSDIR)/hicolor/$$png/apps; \
	convert isodumper.png -geometry $$png $(DESTDIR)$(ICONSDIR)/hicolor/$$png/apps/isodumper.png; \
	done
	mkdir -p $(DESTDIR)$(ICONSDIR)/hicolor/scalable/apps
	install -m 644 isodumper.svg $(DESTDIR)$(ICONSDIR)/hicolor/scalable/apps/isodumper.svg


	# for isodumper.mo translations
	for locale in share/locale/*; \
	do \
	mkdir -p $(DESTDIR)$(LOCALEDIR)/$$locale/LC_MESSAGES; \
	install -m 644 $$locale/LC_MESSAGES/isodumper.mo $(DESTDIR)$(LOCALEDIR)/$$locale/LC_MESSAGES/isodumper.mo; \
	done

tar:
	tar cvJf isodumper.tar.xz *

README.txt: README.md
	pandoc -f markdown -t plain README.md -o README.txt


i18n.txt: i18n.md
	pandoc -f markdown -t plain i18n.md -o i18n.txt
