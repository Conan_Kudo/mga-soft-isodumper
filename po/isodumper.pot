# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the isodumper package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: isodumper 1.04\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-12-15 07:28+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: backend/raw_write.py:49 backend/raw_write.py:78
msgid "Reading error."
msgstr ""

#: backend/raw_write.py:58
msgid "You don't have permission to write to the device {}"
msgstr ""

#: backend/raw_write.py:87 backend/raw_write.py:100 backend/raw_write.py:108
msgid "Writing error."
msgstr ""

#: backend/raw_write.py:117 lib/isodumper.py:340
msgid "Success"
msgstr ""

#: backend/raw_write.py:122
msgid "No partition is mounted."
msgstr ""

#: backend/raw_write.py:128
msgid "Could not read mtab ! {} {}"
msgstr ""

#: backend/raw_write.py:131
msgid "Unmounting all partitions of  {}:\n"
msgstr ""

#: backend/raw_write.py:134
msgid "Trying to unmount {}...\n"
msgstr ""

#: backend/raw_write.py:138
msgid "Partition {} is busy"
msgstr ""

#: backend/raw_write.py:140
msgid "Error, umount {} was terminated by signal {}"
msgstr ""

#: backend/raw_write.py:142
msgid "{} successfully unmounted"
msgstr ""

#: backend/raw_write.py:144
msgid "Error, umount returned {}"
msgstr ""

#: backend/raw_write.py:146
msgid "Execution failed: {}"
msgstr ""

#: backend/raw_write.py:171
msgid "Signature file {} not found\n"
msgstr ""

#: backend/raw_write.py:179
msgid "Sum SHA512 file {} not found\n"
msgstr ""

#: backend/raw_write.py:207
#, python-format
msgid "Invalid signature for %s.sha512"
msgstr ""

#: backend/raw_write.py:209
msgid "SHA512 sum: {}"
msgstr ""

#: backend/raw_write.py:213
msgid "The sha512 sum check is OK and the sum is signed"
msgstr ""

#: backend/raw_write.py:215
msgid "The sha512 sum check is OK but the signature can't be found"
msgstr ""

#: backend/raw_write.py:217
msgid "/!\\The computed and stored sums don't match"
msgstr ""

#: lib/isodumper.py:131
msgid "Mb"
msgstr ""

#: lib/isodumper.py:152
msgid "Target Device: "
msgstr ""

#: lib/isodumper.py:170
msgid "Formatting confirmation"
msgstr ""

#: lib/isodumper.py:175
msgid "The device was formatted successfully."
msgstr ""

#: lib/isodumper.py:179
msgid "An error occurred while creating a partition."
msgstr ""

#: lib/isodumper.py:183
msgid "Authentication error."
msgstr ""

#: lib/isodumper.py:187
msgid "An error occurred."
msgstr ""

#: lib/isodumper.py:204
msgid "Wrote: {}% "
msgstr ""

#: lib/isodumper.py:230
msgid "Backup confirmation"
msgstr ""

#: lib/isodumper.py:230
msgid "Do you want to overwrite the file?"
msgstr ""

#: lib/isodumper.py:238
#, python-format
msgid ""
"The destination directory is too small to receive the backup (%s Mb needed)"
msgstr ""

#: lib/isodumper.py:244 lib/isodumper.py:502 lib/isodumper.py:660
msgid "Backup to:"
msgstr ""

#: lib/isodumper.py:256
#, python-brace-format
msgid "{source} successfully written to {target}"
msgstr ""

#: lib/isodumper.py:277
msgid "The device is too small to contain the ISO file."
msgstr ""

#: lib/isodumper.py:281
msgid "Writing confirmation"
msgstr ""

#: lib/isodumper.py:284
msgid "The device is bigger than 32 Gbytes. Are you sure you want use it?"
msgstr ""

#: lib/isodumper.py:284 lib/isodumper.py:352
msgid "Warning"
msgstr ""

#: lib/isodumper.py:291
#, python-brace-format
msgid "Writing {source} to {target}"
msgstr ""

#: lib/isodumper.py:292
msgid " to "
msgstr ""

#: lib/isodumper.py:292
msgid "Executing copy from "
msgstr ""

#: lib/isodumper.py:308
#, python-brace-format
msgid "Image {source} successfully written to {target}"
msgstr ""

#: lib/isodumper.py:309
msgid "Bytes written: "
msgstr ""

#: lib/isodumper.py:310
msgid "Checking "
msgstr ""

#: lib/isodumper.py:325
msgid "Adding persistent partition"
msgstr ""

#: lib/isodumper.py:327
msgid "Added persistent partition"
msgstr ""

#: lib/isodumper.py:340
msgid ""
"The operation completed successfully.\n"
" You are free to unplug it now, a logfile \n"
"(/home/-user- or /root)/.isodumper/isodumper.log will be saved when\n"
" you close the application."
msgstr ""

#: lib/isodumper.py:352
msgid ""
"Writing is in progress. Exiting during writing \n"
"            will make the device or the backup unusable.\n"
"            Are you sure you want to quit during writing?"
msgstr ""

#: lib/isodumper.py:364
msgid "Error"
msgstr ""

#: lib/isodumper.py:404
msgid "Image "
msgstr ""

#: lib/isodumper.py:408 lib/isodumper.py:459
msgid "IsoDumper"
msgstr ""

#: lib/isodumper.py:408
msgid ""
"Mageia IsoDumper<BR />----------------<BR />This GUI program is primarily "
"for safely writing a bootable ISO image to a USB flash drive, an operation "
"devious & potentially hazardous when done by hand. As a bonus, it can also "
"back up theentire previous<BR />contents of the flash drive onto the hard "
"disc, and restore the flash drive to its previous state subsequently.<BR /"
">It gives also a feature for formatting the USB device.<BR /><BR />IsoDumper "
"can be launched either from the menus, or a user or root console with the "
"command 'isodumper'.<BR />For normal users, the root password is solicited; "
"this is necessary for the program's operation. <BR />The flash drive can be "
"inserted beforehand or once the program is started. In the latter case, a "
"dialogue will say that there is no flash drive inserted, and allow a 'retry' "
"to find it once it is. <BR />(You may have to close any automatically opened "
"File Manager window).<BR /><BR />The fields of the main window are as "
"follows:<BR />- Device to work on: the device of the USB flash drive, a drop-"
"down list to choose from.<BR />- Write Image: to choose the source ISO image "
"*.iso (or flash drive backup file *.img) to write out.<BR />- Write to "
"device: This button launches the operation - with a prior warning dialogue. "
"<BR />The operation is shown in the progress bar beneath.<BR />- Backup to: "
"define the name and placement of the backup image file. The current flash "
"drive will be backed up to a disc file. Note that the entire flash drive is "
"preserved, regardless of its actual contents; ensure that you have the "
"necessary free disc space (the same size as the USB device). This backup "
"file can be used later to restore the flash drive by selecting it as the "
"source *.img file to write out.<BR />- Backup the device: launch the backup "
"operation.<BR />- Format the device:  create an unique partition on the "
"entire volume in the specified format in FAT, NTFS or ext. You can specify a "
"volume name and the format in a new dialog box.<BR />"
msgstr ""

#: lib/isodumper.py:451
msgid "Choose an image"
msgstr ""

#: lib/isodumper.py:452
msgid ""
"Warning\n"
"This will destroy all data on the target device,\n"
"        are you sure you want to proceed?\n"
"        If you say ok here, please <b>do not unplug</b>        the device "
"during the following operation."
msgstr ""

#: lib/isodumper.py:486
msgid "Device to work on:"
msgstr ""

#: lib/isodumper.py:490
msgid "Write Image:"
msgstr ""

#: lib/isodumper.py:496
msgid "&Write to device"
msgstr ""

#: lib/isodumper.py:499
msgid "Add a persistent partition in the remaining space"
msgstr ""

#: lib/isodumper.py:508
msgid "Backup the device"
msgstr ""

#: lib/isodumper.py:511
msgid "Format the device in FAT, NTFS or ext:"
msgstr ""

#: lib/isodumper.py:513
msgid "Format the device"
msgstr ""

#: lib/isodumper.py:516
msgid "Progress"
msgstr ""

#: lib/isodumper.py:520
msgid "Report"
msgstr ""

#: lib/isodumper.py:523 lib/isodumper.py:628
msgid "Refresh"
msgstr ""

#: lib/isodumper.py:525
msgid "About"
msgstr ""

#: lib/isodumper.py:527
msgid "Help"
msgstr ""

#: lib/isodumper.py:529
msgid "Quit"
msgstr ""

#: lib/isodumper.py:535
msgid "UDisks2 is not available on your system"
msgstr ""

#: lib/isodumper.py:551
msgid "Label for the device:"
msgstr ""

#: lib/isodumper.py:555
msgid "FAT 32 (Windows)"
msgstr ""

#: lib/isodumper.py:557
msgid "NTFS (Windows)"
msgstr ""

#: lib/isodumper.py:559
msgid "ext4 (Linux)"
msgstr ""

#: lib/isodumper.py:561
msgid "Execute"
msgstr ""

#: lib/isodumper.py:562 lib/isodumper.py:629
msgid "Cancel"
msgstr ""

#: lib/isodumper.py:619
msgid "A tool for writing ISO images to  a device"
msgstr ""

#: lib/isodumper.py:627
msgid ""
"Warning\n"
"No target devices were found.\n"
"You need to plug in a USB Key to which the image can be written."
msgstr ""

#: polkit/org.mageia.isodumper.policy.in.h:1
msgid "Isodumper requesting write access"
msgstr ""

#: polkit/org.mageia.isodumper.policy.in.h:2
msgid "Manatools requesting write access"
msgstr ""

#: share/applications/format-usb.desktop.in.h:1
msgid "A GUI tool to format USB sticks"
msgstr ""

#: share/applications/isodumper.desktop.in.h:1
msgid "A GUI tool to write .img and .iso files to USB sticks"
msgstr ""
