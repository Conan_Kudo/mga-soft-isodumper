Installation
=======
make install

Modules Requirements
==============
- sys
- imp
- yui
- time
- os
- re
- gettext
- subprocess 
- pydbus
- gi.repository
- hashlib
- gnupg
- threading
- logging
- parted

Reminders
======
Install magiback in /usr/bin
Install org.mageia.magiback.service in /usr/share/dbus-1/system-services/
Install org.mageia.Magiback.conf /etc/dbus-1/system.d/
Install polkit policy file  in /usr/share/polkit-1/actions/
Install raw_format.py in /usr/lib/isodumper as raw_format

